/* $Id: */

-- SUMMARY --

Instead of sending out emails, email tester display all sent emails in a modal
frame.

Thanks to this module you don't annoy each person of your company because you
are doing testing. You can also redirect all outgoing emails to one email if
you want to be sure SMTP config is OK.

Email tester is added in the package devel and you can access configuration
through devel config page:
http://emailtester.actency.fr/sites/default/files/email_tester_config.jpg

Permission has been set for the module, if someone doesn�t have the permission
to see outgoing emails, he won�t see anything and he won�t receive any emails.
You have to disable �emails in modal frame� in devel settings.

-- REQUIREMENTS --

* Devel
* jQuery UI


-- INSTALLATION --

* Install as usual, see http://drupal.org/node/70151 for further information.


-- CONFIGURATION --

* Configure user permissions in Administer >> User management >> Permissions >>
  email_tester module:

  - access email tester information

    Users in roles with the "access email tester information" permission will
    see all sent emails in a modal frame.

* Choose if you want to disable email displaying in a modalframe:
  Administrer >> Site configuration >> Devel settings >> SMTP Library

* You can also reroute all outgoing emails in:
  Administrer >> Site configuration >> Devel settings >> Reroute email


-- CONTACT --

Current maintainers:
* Jerome Megel - http://drupal.org/user/707854

This project has been sponsored by:
* Actency
  http://www.actency.fr
